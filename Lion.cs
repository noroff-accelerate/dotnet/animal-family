﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimalFamily
{
    public class Lion : Animal, IPreditor, IClimber
    {
        public void Climb()
        {
            Console.WriteLine($"Climby stuffs");
        }

        public override void MakeNoise()
        {
            Console.WriteLine($"raaawr");
        }

        public void Prey()
        {
            Console.WriteLine($"Antelope");
        }

        public override void Sleep()
        {
            Console.WriteLine("zzzz grmblgmbrl zzz");
        }

    }
}
