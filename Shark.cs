﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimalFamily
{
    public class Shark : Animal, IPreditor
    {
        public int Length { get; set; }

        public override void MakeNoise()
        {
            Console.WriteLine($"grbrgrbllrb");
        }

        public override void Sleep()
        {
            Console.WriteLine("..... yeah I don't sleep");
        }

        public void Prey()
        {
            Console.WriteLine($"Fish and sometime peeps");
        }


    }
}
