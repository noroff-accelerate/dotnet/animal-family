﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnimalFamily
{
    public abstract class Animal
    {
        public string Name { get; set; }
        public double Weight { get; set; }

        // Biomes enum used here as a type for this property
        public Biomes Biome { get; set; }

        public void Present()
        {
            Console.WriteLine($"This animal is {Name}, weighs {Weight} and lives around {Biome}");
        }

        public virtual void Sleep()
        {
            Console.WriteLine("zzzzzzzzz");
        }

        public abstract void MakeNoise();
    }
}
