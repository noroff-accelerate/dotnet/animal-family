﻿using System;

namespace AnimalFamily
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Shark sharkObject = new Shark() 
            {
                Name = "Bruce", 
                Length = 24, 
                Weight = 109.2,
                // set the enum
                Biome = Biomes.Ocean
            };

            Lion lionObject = new Lion()
            {
                Name = "Simba",
                Weight = 87.2,
                // set the enum
                Biome = Biomes.Plains
            };


            sharkObject.Sleep();
            sharkObject.MakeNoise();
            sharkObject.Prey();
            sharkObject.Present();

            lionObject.Sleep();
            lionObject.MakeNoise();
            lionObject.Prey();
            lionObject.Present();
            lionObject.Climb();
        }
    }
}
