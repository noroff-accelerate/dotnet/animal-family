

<p align="center">
  <a href="#key-features">Key Features</a>
  <a href="#how-to-use">How To Use</a>
</p>


## Key Features

Demonstrates OOP features by solving the try it out functionality below

- Make an animal class
- Give it some fields
- Name
- Weight
- Biome – where it lives (ocean, desert)
- Give it some methods
- Present – writes a string explaining what the animal is using the three fields.
- MakeNoise


- Make your previous animal class abstract
- MakeNoise needs to be abstract
- Create a virtual method called Sleep
- Default implementation is just printing the animal is sleeping
- Create a Lion and Shark class which inherit from animal.
- Implement the MakeNoise as best you can think these animals would.
- Override the Sleep method to show how the animals sleep differently.
- Add a length field to the Shark.

- Replace the biome field in Animal with an enumerator holding some specific values.


## How To Use

To clone and run this application, you'll need [Git](https://git-scm.com)
From your command line:

```bash
# Clone this repository
$ git clone https://gitlab.com/noroff-accelerate/dotnet/animal-family.git
```

2) Go into the repository, Open the solution in VS

3) Run the console app in VS


